const express = require('express');
const helmet = require('helmet');
const cors = require('cors');

const clienteRoutes = require('./routes/cliente.routes');
/*const cuentaRoutes = require('./routes/cuenta.routes');
const chequeraRoutes = require('./routes/chequera.routes');
const chequeRoutes = require('./routes/cheque.routes');
*/
//Se cargan las variables de entorno
require('dotenv').config();

//Configuración del server
const app = express();
app.use(cors());
app.use(helmet());
app.use(express.urlencoded({ extended: false }));
app.use(express.json({limit: '50mb'}));




//Se agrega el servicio
app.use('/api/cliente', clienteRoutes);
/*app.use('/api/cuenta', cuentaRoutes);
app.use('/api/chequera', chequeraRoutes);
app.use('/api/cheque', chequeRoutes);
*/

//Iniciar el servicio
app.listen(process.env.PORT, process.env.HOST, () => {
    console.info(`Listen to http://${process.env.HOST}:${process.env.PORT}`);
});
