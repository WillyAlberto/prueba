const express = require('express');
const router = express.Router();
const executeSQL = require('../database');

router.get('/', (req, res) => {
    let query = 'SELECT * FROM Cuenta'
    
    return executeSQL(query, (err, data) => {
        if (err)
            return res.status(500).json({ msg: 'No se pudo recuperar la informacion' });
        
        console.log(data)
        return res.status(200).json(data);
    });
});
