const express = require('express');
const router = express.Router();
const executeSQL = require('../database');

router.get('/:id?', (req, res) => {
    const id = req.params.id;
    let query = 'SELECT * FROM Cliente'
    
    if(id) query += ' WHERE IdCliente = ' + id
    console.log(query)
    
    return executeSQL(query, (err, data) => {
        if (err)
            return res.status(500).json({ msg: 'No se pudo recuperar la informacion' });
        
        //console.log(data)
        return res.status(200).json(data);
    });
});

router.post('/', (req, res) => {
    const body = req.body;

    let query = 'INSERT INTO Cliente VALUES('
    query += "'" + body.nombre + "',"
    query += "'" + body.apellido + "',"
    query += "'" + body.numero_identificacion + "',"
    query += "'" + body.fecha_nacimiento + "',"
    query += "'" + body.direccion + "',"
    query += "'" + body.email + "',"
    query += "'" + body.telefono + "')"
    
    console.log(query)

    return executeSQL(query, (err, data) => {
        if (err)
            return res.status(500).json({ msg: 'No se pudo recuperar la informacion' });
        
        console.log(data)
        return res.status(200).json({ msg: 'Cliente registrado con exito.' });
    });
    return res.status(200).json({ msg: 'Entro cliente/agregar'});
});

router.put('/', (req, res) => {
    const body = req.body;
    const id = body.IdCliente;

    let query = 'UPDATE Cliente SET '
    query += "nombre = '" + body.nombre + "',"
    query += "apellido = '" + body.apellido + "',"
    query += "numero_identificacion = '" + body.numero_identificacion + "',"
    query += "fecha_nacimiento = '" + body.fecha_nacimiento + "',"
    query += "direccion = '" + body.direccion + "',"
    query += "email = '" + body.email + "',"
    query += "telefono = '" + body.telefono + "' "
    query += "WHERE IdCliente = " + id
    
    console.log(query)

    return executeSQL(query, (err, data) => {
        if (err)
            return res.status(500).json({ msg: 'No se pudo recuperar la informacion' });
        
        console.log(data)
        return res.status(200).json({ msg: 'Cliente actualizado con exito.' });
    });
    
    return res.status(200).json({ msg: 'Entro cliente/agregar'});
});

router.delete('/:id', (req, res) => {
    const id = req.params.id;
    let query = 'DELETE FROM Cliente WHERE IdCliente = ' + id
    console.log(query)

    return executeSQL(query, (err, data) => {
        if (err)
            return res.status(500).json({ msg: 'No se pudo recuperar la informacion' });
        
        console.log(data)
        return res.status(200).json({ msg: 'Cliente eliminado con exito.' });
    });
    return res.status(200).json({ msg: 'Entro cliente/agregar'});
});

module.exports = router;
