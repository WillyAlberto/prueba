const {Connection, Request, TYPES} = require('tedious');
const { route } = require('./routes/cliente.routes');

//Configuración para la conexion a la DB
var config = {  
    server: 'db',  //update me
    authentication: {
        type: 'default',
        options: {
            userName: 'sa', //update me
            password: 'Test1234'  //update me
        }
    },
    options: {
        encrypt: true,
        database: 'PRUEBA',
        trustedConnection: true,
        trustServerCertificate: true,
        rowCollectionOnDone: true
    }
};

const executeSQL = async(sql, callback) => {
    let connection = new Connection(config); 

    connection.connect(async (err) => {
        if (err)
            return callback(err, null)
        
        let rows = []
        const request = new Request(sql, (err, rowCount) => {
            if(err) {
                return callback(err, null)
            } else {
                console.log(rowCount + ' rows');
                connection.close();
                return callback(null, rows)
            }
        })

        request.on('row', function(columns){
            let row = {  }
            columns.forEach(function(column) {
                row[column.metadata.colName] = column.value
            });
            rows.push(row)
        });
    
        connection.execSql(request)
    });
}

module.exports = executeSQL;