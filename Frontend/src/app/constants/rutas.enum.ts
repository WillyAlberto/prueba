export enum RUTAS {
    _home = '',
    _clientes = 'clientes',
    _cuentas = 'cuentas',
    _chequeras = 'chequeras',
    _cheques = 'cheques',
    _documentacion = 'documentacion',
    _nuevo = 'nuevo',
    _editar = 'editar/:id',
    _editarBase = 'editar/',
    _visualizar = 'info/:id',
    _visualizarBase = 'info/'
}