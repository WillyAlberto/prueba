export enum MSG {
    _Exito = 'success',
    _Info = 'info',
    _Advertencia = 'warn',
    _Error = 'error',
    _Personal = 'custom'
}