import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-toolbar-table',
  templateUrl: './toolbar-table.component.html',
  styles: [
  ]
})
export class ToolbarTableComponent {

  @Input() path: string = '';
  @Input() selectedRows: Array<any> | undefined;
  @Input() data: any;

  @Output() delete = new EventEmitter();

  constructor(protected generalService: GeneralService) { }

  deleteSelected() {
    this.delete.emit();
  }
}
