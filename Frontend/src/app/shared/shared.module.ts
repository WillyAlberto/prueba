import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarTableComponent } from './toolbar-table/toolbar-table.component';

import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ListboxModule } from 'primeng/listbox';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ToolbarModule } from 'primeng/toolbar';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'primeng/fileupload';
import { RouterModule } from '@angular/router';
import { InputSwitchModule } from 'primeng/inputswitch';

@NgModule({
  declarations: [
    ToolbarTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,

    AutoCompleteModule,
    ButtonModule,
    FileUploadModule,
    InputTextModule,
    ListboxModule,
    ToolbarModule,
    InputSwitchModule
  ],
  exports: [
    ToolbarTableComponent
  ]
})
export class SharedModule { }