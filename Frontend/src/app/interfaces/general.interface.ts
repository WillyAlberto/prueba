export interface IInfo {
    nombre: string;
    valor: string;
}

export type IForm<T> = {
    [K in keyof T]?: any;
}