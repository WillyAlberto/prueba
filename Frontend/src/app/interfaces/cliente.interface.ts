export interface ICliente {
    IdCliente?: Number;
    nombre: String;
    apellido: String;
    numero_identificacion: Number;
    fecha_nacimiento: Date;
    direccion: String;
    email: String;
    telefono: Number;
}