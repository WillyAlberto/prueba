import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RUTAS } from 'src/app/constants/rutas.enum';
import { ClientesComponent } from './clientes.component';
import { CrearClienteComponent } from './crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';

const routes: Routes = [
  { path: RUTAS._home, component:  ClientesComponent },
  { path: RUTAS._nuevo, component:  CrearClienteComponent },
  { path: RUTAS._editar, component:  EditarClienteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivosRoutingModule { }
