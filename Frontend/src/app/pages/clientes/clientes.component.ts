import { Component, OnInit } from '@angular/core';

import { ConfirmationService, MessageService } from 'primeng/api';
import { ClienteService } from 'src/app/services/cliente.service';
import { ICliente } from 'src/app/interfaces/cliente.interface';
import { RUTAS } from 'src/app/constants/rutas.enum';
import { GeneralService } from 'src/app/services/general.service';
import { MSG } from 'src/app/constants/mensaje.enum';
import { DataService } from 'src/app/services/cuenta.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styles: [
  ]
})
export class ClientesComponent implements OnInit {
  
  //Menu
  eliminados = false;

  //Tabla
  titleTable: string = 'Clientes';
  tableData: ICliente[];
  selectedRows: ICliente[];
  selectedColumns: any[] = [];
  columnsToFilter: any[] = [];

  constructor(
    private confirmationService: ConfirmationService, protected generalService: GeneralService,
    private clienteService: ClienteService,
  ) {
  }

  ngOnInit(): void {
    this.clienteService.getClientes()
    .subscribe(
      data => {
        this.tableData = data        
        if(data[0]) {
          let claves = Object.keys(this.tableData[0]).filter(item=> item!=='tipo' && item!=='asignado' && item!=='descripcion');
          this.selectedColumns = claves;
        }
      },
      error => this.generalService.addMsg(MSG._Error, 'Error', 'No se pudieron recuperar los datos.')     
    );
  }
  
  deleteSelected(row?: any) {
    const title = row? this.titleTable.substring(0,this.titleTable.length - 1): this.titleTable;
    
    this.confirmationService.confirm({
      key: 'confirmDialog',
      header: `Eliminar clientes seleccionados`,
      message: `¿Esta seguro de dar de baja a los clientes indicados.`,
      icon: 'pi pi-exclamation-triangle',

      accept: () => {
        if(row) {
          this.clienteService.deleteCliente(row.IdCliente)
          .subscribe(
            result => {
              const index = this.tableData.indexOf(row);
              if(!this.eliminados) this.tableData.splice(index, 1);
              this.generalService.addMsg(MSG._Exito, 'Eliminar', result.msg)
            },
            err => this.generalService.addMsg(MSG._Error, 'Error', `No se pudo eliminar.`)
          )
        }
        else {
          this.selectedRows.map(row => {
            this.clienteService.deleteCliente(row.IdCliente).subscribe(
              result => {
                const index = this.tableData.indexOf(row);
                if(!this.eliminados) this.tableData.splice(index, 1);
                this.generalService.addMsg(MSG._Exito, 'Eliminar', result.msg)
              },
              err => this.generalService.addMsg(MSG._Error, 'Error', `No se pudo eliminar.`)
            )
          });
        }
      },
      reject: () => { 
        this.generalService.addMsg(
          MSG._Info, 'Cancelado', `No se ha confirmado la eliminación.`);
      }
    });
  }

  Eliminados(event){
    this.eliminados = event;
    this.ngOnInit()
  }
}
