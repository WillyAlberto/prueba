import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientesComponent } from './clientes.component';
import { ActivosRoutingModule } from './clientes-routing.module';
import { CrearClienteComponent } from './crear-cliente/crear-cliente.component';
import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';
import { SharedModule } from 'src/app/shared/shared.module';

import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { DividerModule } from 'primeng/divider';
import { TabViewModule } from 'primeng/tabview';
import { TimelineModule } from 'primeng/timeline';
import { DialogModule } from 'primeng/dialog';
import { ConfirmationService } from 'primeng/api';
import { CardModule } from 'primeng/card';

import { AutoCompleteModule } from 'primeng/autocomplete';
import { SelectButtonModule } from 'primeng/selectbutton';

@NgModule({
  declarations: [
    ClientesComponent,
    CrearClienteComponent,
    EditarClienteComponent,
  ],
  imports: [
    CommonModule,
    ActivosRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,

    ToastModule,
    ToolbarModule,
    SelectButtonModule,
    AutoCompleteModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    ConfirmDialogModule,
    DropdownModule,
    DividerModule,
    TabViewModule,
    TimelineModule,
    DialogModule,
    CardModule
  ],
  providers: [ConfirmationService]
})
export class ActivosModule { }
