import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente.service';
import { GeneralService } from 'src/app/services/general.service';
import { MSG } from 'src/app/constants/mensaje.enum';
import { IForm } from 'src/app/interfaces/general.interface';
import { ICliente } from 'src/app/interfaces/cliente.interface';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styles: [
  ]
})
export class EditarClienteComponent implements OnInit {
  
  formCliente: UntypedFormGroup;

  constructor(
    private rutaActiva: ActivatedRoute, 
    private generalService: GeneralService,
    private clienteService: ClienteService,
    protected formBuilder: UntypedFormBuilder
  ) {

    let Cliente:IForm<ICliente> = {
      IdCliente: [null, Validators.required],
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      numero_identificacion: [null, Validators.required],
      fecha_nacimiento: [null, Validators.nullValidator],
      direccion: ['', Validators.required], 
      email: ['', Validators.required],
      telefono: [null, Validators.required]
    };
    this.formCliente = formBuilder.group(Cliente);
  }
  Regresar= (): void => this.generalService.setRutaPadre()
  
  //Carga de los datos iniciales
  ngOnInit(): void {
    const id = this.rutaActiva.snapshot.params['id'];

    this.clienteService.getClienteByID(id)
    .subscribe(
      cliente => {
        this.formCliente.setValue(cliente[0])
      }
    );
  }
  
  //Guarda el nuevo activo en la DB
  Guardar(): void {
    if(this.formCliente.valid) {
      let cliente = this.formCliente.getRawValue();
     
      this.clienteService.putCliente(cliente)
      .subscribe(
        result => {
          this.generalService.addMsg(MSG._Exito,'EXITO','Cliente actualizado exitosamente.')
          this.generalService.setRutaPadre()
        },
        err => this.generalService.addMsg(MSG._Error,'FALLO','Servicio no disponible.')
      );

    }
    else {
      this.generalService.addMsg(MSG._Error,'Error','Faltan campos obligatorios.')
    } 
  }

}
