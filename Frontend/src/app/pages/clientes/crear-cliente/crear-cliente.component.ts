import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { MSG } from 'src/app/constants/mensaje.enum';
import { ICliente } from 'src/app/interfaces/cliente.interface';
import { IForm } from 'src/app/interfaces/general.interface';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-crear-cliente',
  templateUrl: './crear-cliente.component.html',
  styles: [
  ]
})
export class CrearClienteComponent implements OnInit {
  
  formCliente: UntypedFormGroup;

  constructor(
    private generalService: GeneralService,
    private clienteService: ClienteService,
    protected formBuilder: UntypedFormBuilder
  ) {

    let Cliente:IForm<ICliente> = {
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      numero_identificacion: [null, Validators.required],
      fecha_nacimiento: [null, Validators.nullValidator],
      direccion: ['', Validators.required], 
      email: ['', Validators.required],
      telefono: [null, Validators.required]
    };
    this.formCliente = formBuilder.group(Cliente);
  }
  Regresar= (): void => this.generalService.setRutaPadre()
  
  //Carga de los datos iniciales
  ngOnInit(): void {
  }
  
  //Guarda el nuevo activo en la DB
  Crear(): void {
    console.log(this.formCliente.getRawValue())
    if(this.formCliente.valid) {

      let cliente = this.formCliente.getRawValue();
     
      this.clienteService.postCliente(cliente)
      .subscribe(
        result => {
          this.generalService.addMsg(MSG._Exito,'EXITO','Cliente registrado exitosamente.')
          this.generalService.setRutaPadre()
        },
        err => this.generalService.addMsg(MSG._Error,'FALLO','Servicio no disponible.')
      );
    }
    else {
      this.generalService.addMsg(MSG._Error,'Error','Faltan campos obligatorios.')
    } 
  }
  
}
