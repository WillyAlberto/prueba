import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { ICliente } from '../interfaces/cliente.interface';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private _URL = `http://${environment.server}:${environment.port}`;

  constructor(private http: HttpClient) {}

  //GET
  getClientes() {
    return this.http.get<any>(`${this._URL}/api/cliente/`);
  }

  //GET
  getClienteByID(id: Number) {
    return this.http.get<any>(`${this._URL}/api/cliente/${id}/`);
  }

  //POST
  postCliente(body: ICliente) {
    return this.http.post<any>(`${this._URL}/api/cliente/`,body);
  }

  //PUT
  putCliente(body: ICliente) {
    return this.http.put<any>(`${this._URL}/api/cliente/`,body);
  }

  //DELETE
  deleteCliente(id: Number) {
    return this.http.delete<any>(`${this._URL}/api/cliente/${id}/`);
  }

}
