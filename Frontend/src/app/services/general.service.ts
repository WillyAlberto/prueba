import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { RUTAS } from '../constants/rutas.enum';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor(
    private router: Router, private rutaActiva: ActivatedRoute,
    private messageService: MessageService, 
  ) {
  }

  setRutaPadre(): void {
    const actual = this.router.url;
    
    if(actual.includes(RUTAS._clientes)) this.router.navigate([RUTAS._clientes]);
    else if(actual.includes(RUTAS._cuentas)) this.router.navigate([RUTAS._cuentas]);
    else if(actual.includes(RUTAS._chequeras)) this.router.navigate([RUTAS._chequeras]);
    else if(actual.includes(RUTAS._cheques)) this.router.navigate([RUTAS._cheques]);
  }

  appendRuta(tipo: string, id?: number): void {
    let path: string = '';
    if(tipo==='visualizar') path = `${RUTAS._visualizarBase}/${id}`
    else if(tipo==='editar') path = `${RUTAS._editarBase}/${id}`
    else path = RUTAS._nuevo

    this.router.navigate([`${this.router.url}/${path}`]);
  }
  
  addMsg(severidad: string, titulo: string, mensaje: string, regresar: Boolean = false, icon = undefined): void {
    this.messageService.add({
      severity: severidad,
      summary:titulo,
      detail: mensaje,
      icon: ''
    });
    if(regresar) setTimeout(() => this.setRutaPadre(), 700);
  }
}
