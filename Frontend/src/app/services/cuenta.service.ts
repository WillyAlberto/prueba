import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Header } from 'primeng/api';
import { Observable } from 'rxjs';

import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _URL = `http://${environment.server}:${environment.port}`;

  constructor(private http: HttpClient) {}

  
}
