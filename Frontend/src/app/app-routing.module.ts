import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppMainComponent } from './app.main.component';
import { RUTAS } from './constants/rutas.enum';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: AppMainComponent,
    children: [
      { path: '', component: DashboardComponent },
      {
        path: RUTAS._clientes,
        loadChildren: () => import('./pages/clientes/clientes.module').then(m => m.ActivosModule)
      },
     /* {
        path: RUTAS._accesorios,
        loadChildren: () => import('./pages/accesorios/accesorios.module').then(m => m.AccesoriosModule)
      },
      {
        path: RUTAS._mantenimientos,
        loadChildren: () => import('./pages/mantenimientos/mantenimientos.module').then(m => m.MantenimientosModule)
      },
      { path: RUTAS._reportes, component: ReportesComponent },*/
    ]
  },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
