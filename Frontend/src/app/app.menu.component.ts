import { Component, OnInit } from '@angular/core';
import { AppMainComponent } from './app.main.component';
import { RUTAS } from './constants/rutas.enum';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {
    model: any[] = [];

    constructor(public appMain: AppMainComponent) {}

    ngOnInit() {
        this.model = [
            { label: 'Dashboard', icon: 'pi pi-home', routerLink: [RUTAS._home] },
            { label: 'Clientes', icon: 'pi pi-fw pi-list', routerLink: [RUTAS._clientes] },
            { label: 'Cuentas', icon: 'pi pi-fw pi-list', routerLink: [RUTAS._cuentas] },
            { label: 'Chequera', icon: 'pi pi-fw pi-list', routerLink: [RUTAS._chequeras] },
            { label: 'Cheque', icon: 'pi pi-fw pi-list', routerLink: [RUTAS._cheques] },
//            { label: 'Documentacion', icon: 'pi pi-fw pi-info-circle', routerLink: [RUTAS._documentacion] }
        ];
    }
}
