create DATABASE PRUEBA

create table Cliente(
	IdCliente int identity(1,1) not null primary key,
	nombre varchar(60) not null,
	apellido varchar(60) not null,
	numero_identificacion varchar(15) not null,
	fecha_nacimiento date not null,
	direccion varchar(100) not null,
	email varchar(100) not null,
	telefono varchar(8) not null
)

create table Chequera(
	IdChequera int identity(1,1) not null primary key,
	cantidad int not null,
	estado char not null,
	IdCliente int not null,
	FOREIGN KEY (IdCliente) REFERENCES Cliente(IdCliente)
)

create table Cuenta(
	IdCuenta int identity(1,1) not null primary key,
	tipo bit not null,
	monto varchar(60) not null,
	estado char not null,
	IdChequera int null,
	IdCliente int not null,
	FOREIGN KEY (IdChequera) REFERENCES Chequera(IdChequera),
	FOREIGN KEY (IdCliente) REFERENCES Cliente(IdCliente)
)

create table Cheque(
	IdCheque int identity(1,1) not null primary key,
	IdChequera int not null,
	FOREIGN KEY (IdChequera) REFERENCES Chequera(IdChequera)
)